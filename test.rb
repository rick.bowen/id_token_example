#!/usr/bin/env ruby
# vim: ts=2 noet:
require "jwt"
require "net/http"
require "json"
require "pp"

id_token = JWT.decode(ENV["ID_TOKEN_1"], nil, false)[0]
server_url = ENV["CI_SERVER_URL"]

url = URI("#{server_url}/.well-known/openid-configuration")
http = Net::HTTP.new(url.host, url.port)
if server_url.start_with?("https")
  http.use_ssl = true
end

request = Net::HTTP::Get.new(url)
response = http.request(request)
openid_configuration = JSON.parse(response.body)

data = {
  "id_token": {
    "issuer": id_token["iss"],
    "aud": id_token["aud"],
  },
  "openid-configuration": {
    "issuer": openid_configuration["issuer"],
    "jwks_uri": openid_configuration["jwks_uri"]
  }
}

pp data